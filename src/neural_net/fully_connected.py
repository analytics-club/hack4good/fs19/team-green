"""
Example Usage: python -m src.neural_net.fully_connected
"""
import os
import time
import numpy as np
import tensorflow as tf


class fullyConnectedNetwork():
    def __init__(self):
        self.loss = None
        self.handle = None
        self.output = None
        self.init_lr = None
        self.accuracy = None
        self.train_op = None
        self.nr_epochs = None
        self.batch_size = None
        self.input_label = None
        self.accuracy_op = None
        self.input_features = None
        self.fraction_end_lr = None
        self.training_handle = None
        self.training_dataset = None
        self.training_iterator = None
        self.prediction_handle = None
        self.prediction_iterator = None
        self.prediction_dataset = None

        self.train_step_float = 0
        self.train_step = tf.Variable(0, name='train_step', trainable=False)
        self.train_epoch = 0

        self.sess = tf.Session()

    def train(self, input_data, input_label, batch_size, nr_epochs, init_lr, fraction_end_lr):
        """
        Creates the training pipeline for the neural network.

        :param input_data: numpy array containing the sample with the corresponding features
        :param input_label: int numpy array containing the labels (0-8)
        :param batch_size: batch size for training
        :param nr_epochs: defines after how many epochs the training should stop
        :param init_lr: initial learning rate
        :param fraction_end_lr: fraction of the initial learning rate, which is the learning rate at the end of training
        :return:
        """
        self.nr_epochs = nr_epochs
        self.init_lr = init_lr
        self.fraction_end_lr = fraction_end_lr

        self.createDataset(input_data, input_label, shuffle_bool=True, batch_size=batch_size)
        self.createIterator()
        self.createModel(self.input_features, self.input_label)
        self.createTrainOp()

        self.sess.run(tf.global_variables_initializer())

        while self.train_epoch < self.nr_epochs:
            self.trainLoop()

    def trainLoop(self):
        """Loops once over the geotif_dataset"""
        self.sess.run(self.training_iterator.initializer)

        total_loss = 0

        while True:
            try:
                ops = [self.train_op, self.loss, self.output]
                _, loss, output = self.sess.run(ops, feed_dict={self.handle: self.training_handle})
                self.train_step_float += 1

                total_loss += loss

            except tf.errors.OutOfRangeError:
                print("Epoch %s    Loss: %s" % (self.train_epoch, "{0:.7f}".format(total_loss)))
                self.train_epoch += 1
                break

    def predict(self, input_data, input_label):
        """Outputs the predictions of the data provided as input"""
        self.createDatasetPrediction(input_data, input_label)
        self.createIteratorPrediction()
        self.sess.run(self.prediction_iterator.initializer)

        predictions = []
        total_loss = 0
        nr_samples = 0
        while True:
            try:
                ops = [self.loss, self.output]
                loss, output = self.sess.run(ops, feed_dict={self.handle: self.prediction_handle})

                predictions.append(np.argmax(output))
                total_loss += loss
                nr_samples += 1

            except tf.errors.OutOfRangeError:
                return np.array(predictions), total_loss / nr_samples

    def createDataset(self, input_data, input_label, shuffle_bool, batch_size):
        """
        Creates based on the two input numpy array a tensorflow geotif_dataset.

        :param input_data: numpy array containing the sample with the corresponding features
        :param input_label: int numpy array containing the labels (0-8)
        :param shuffle_bool: true if geotif_dataset should be shuffled during execution
        :param batch_size: batch size
        :return:
        """
        dataset = tf.data.Dataset.from_tensor_slices((input_data, input_label))

        if shuffle_bool:
            dataset = dataset.shuffle(buffer_size=1000)

        self.training_dataset = dataset.batch(batch_size, drop_remainder=False)

    def createIterator(self):
        """Creates the iterator for the geotif_dataset"""
        with tf.variable_scope('dataset_iterator'):
            self.handle = tf.placeholder(tf.string, shape=[])
            iterator = tf.data.Iterator.from_string_handle(self.handle,
                                                           self.training_dataset.output_types,
                                                           self.training_dataset.output_shapes)
            self.input_features, self.input_label = iterator.get_next()

            self.training_iterator = self.training_dataset.make_initializable_iterator()
            self.training_handle = self.sess.run(self.training_iterator.string_handle())

    def createDatasetPrediction(self, input_data, input_label):
        """
        Creates based on the two input numpy array a tensorflow geotif_dataset.

        :param input_data: numpy array containing the sample with the corresponding features
        :param input_label: int numpy array containing the labels (0-8)
        :return:
        """
        dataset = tf.data.Dataset.from_tensor_slices((input_data, input_label))

        self.prediction_dataset = dataset.batch(1, drop_remainder=False)

    def createIteratorPrediction(self):
        """Creates the iterator for the geotif_dataset"""
        with tf.variable_scope('dataset_iterator_prediciton'):
            self.prediction_iterator = self.prediction_dataset.make_initializable_iterator()
            self.prediction_handle = self.sess.run(self.prediction_iterator.string_handle())

    def createModel(self, input_features, input_label):
        """Creates the model specified by the number of layers and nodes of the fully connected network"""
        fc_hidden_nodes = [512, 512, 4]
        inter_tensor = input_features
        with tf.variable_scope('fully_connected'):
            for nr_nodes in fc_hidden_nodes:
                inter_tensor = tf.contrib.layers.fully_connected(inter_tensor, num_outputs=nr_nodes)

        self.output = inter_tensor
        self.loss = tf.losses.sparse_softmax_cross_entropy(input_label, self.output)

    def createTrainOp(self):
        with tf.name_scope('learning_rate'):
            learning_rate = tf.train.cosine_decay(learning_rate=self.init_lr,
                                                  global_step=self.train_step,
                                                  decay_steps=self.nr_epochs,
                                                  alpha=self.fraction_end_lr)

        self.train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss,
                                                                                     global_step=self.train_step)

    def saveWeights(self):
        saver = tf.train.Saver(max_to_keep=2)
        timestr = time.strftime("%Y%m%d-%H%M%S")
        saver.save(self.sess, os.path.join(os.path.join('results', 'models', 'neural_net', 'weights_' + timestr)))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.sess.close()


def main():
    network = fullyConnectedNetwork()

    dummy_input = np.random.rand(10000, 400)
    dummy_label = np.random.randint(4, size=(10000, 1))

    start_time = time.time()
    network.train(dummy_input, dummy_label, 10, 1, 4e-6, 0.1)
    print('Training Time: %s ' % "{0:.2f}".format(time.time() - start_time))

    predictions, loss = network.predict(dummy_input, dummy_label)
    network.saveWeights()


if __name__ == "__main__":
    main()
