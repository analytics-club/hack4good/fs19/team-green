"""
Example Usage: python -m src.logistic_regression.logistic_regression
"""
import os
import time
import pickle
import numpy as np
import sklearn.linear_model
import sklearn.preprocessing
import sklearn.model_selection

import src.cleanup.cleanup as clp
import src.utils.experiment_report as experiment_report


class LogisticRegression:
    def __init__(self):
        """Wrapper for the sklearn logistic regression model"""
        self.model = None

    def initialiseModel(self):
        """Initialises a new model. An already existing logistic regression model will be overwritten"""
        self.model = sklearn.linear_model.LogisticRegression(penalty='l2',
                                                             tol=1e-4,
                                                             class_weight='balanced')

    def saveModel(self, time_name):
        """Saves the current model"""
        file_name = os.path.join('results', 'models', 'logistic_regression', 'weights_' + time_name)
        with open(file_name, 'wb') as f:
            pickle.dump(self.model, f)

    def loadModel(self, file_path):
        """Loads model specified by file_path. An already existing random forest model will be overwritten"""
        with open(file_path, 'rb') as f:
            self.model = pickle.load(f)


def loadData(feature_list, categorial_list, need_threshold):
    """Loads the data and encodes the categorial variable to an one hot embedding"""
    clean = clp.clean_table(reload_tables=False,
                            in_directory=os.path.join('data', 'raw'),
                            out_directory=os.path.join('data', 'processed'))
    data_input = clean.table.drop_duplicates(subset='HouseholdID', keep='first').dropna(subset=['RespondentSet'])
    data_input = data_input[feature_list]

    for key in categorial_list:
        data_input[key] = data_input[key].astype("category").cat.codes

    index_categorial_features = [i for i in range(len(feature_list)) if feature_list[i] in categorial_list]
    one_hot_encoder = sklearn.preprocessing.OneHotEncoder(categorical_features=index_categorial_features, sparse=False)
    one_hot_data = one_hot_encoder.fit_transform(data_input)

    one_hot_data[one_hot_data[:, -1] <= need_threshold, -1] = 0
    one_hot_data[one_hot_data[:, -1] > need_threshold, -1] = 1

    return one_hot_data


def runExperiment(feature_list, categorial_list, need_threshold):
    """Fits and predicts a random forest model"""
    # ---- Create Writer for Storing a Summary of the experiment ----
    report_writer = experiment_report.ReportWriter(path_dir=os.path.join('results', 'models', 'logistic_regression'))
    report_writer.person_name = 'Francesco'
    report_writer.workstation = 'Francesco Super Computer'
    report_writer.model = 'Logistic Regression'

    # ---- Load and Split Data ----
    data_input = loadData(feature_list, categorial_list, need_threshold)
    random_state = 42
    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(data_input[:, :-1],
                                                                                data_input[:, -1],
                                                                                test_size=0.33,
                                                                                random_state=random_state,
                                                                                shuffle=True)

    # ---- Fit Model ----
    logistic_regressor = LogisticRegression()
    logistic_regressor.initialiseModel()

    start_train_time = time.time()
    logistic_regressor.model.fit(X_train, y_train)
    training_time = time.time() - start_train_time
    print('Training Time: %s ' % "{0:.2f}".format(training_time))

    start_test_time = time.time()
    y_test_predicted = logistic_regressor.model.predict(X_test)
    predict_time = time.time() - start_test_time

    # ---- Save Results ----
    report_writer.feature_list = feature_list
    report_writer.nr_train_samples = X_train.shape[0]
    report_writer.nr_test_samples = X_test.shape[0]
    report_writer.time = time.strftime("%Y%m%d-%H%M%S")

    report_writer.time_training = training_time
    report_writer.time_predict = predict_time

    report_writer.saveReport(y_predicted=y_test_predicted, y_ground_truth=y_test)
    # logistic_regressor.saveModel(time_name=report_writer.time)


def main():
    # Current Features:
    # ['PopulationGroup', 'Status', 'RespondentSet', 'HeadOfHouseholdSex', 'HeadofHouseholdAge',
    #  'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold', 'NoOfHouseholdMembers', 'Wash', 'Shelter',
    #  'Livelihood', 'Recovery', 'Health', 'Nutrition', 'Education', 'Protection']

    # The feature list specifies the feature used to predict if a HH is in need in a specific sector or not.
    # The selected sector to predict is in the last position in the feature_list e.g. 'Wash'
    feature_list = ['PopulationGroup', 'Status', 'RespondentSet', 'HeadOfHouseholdSex', 'HeadofHouseholdAge',
                    'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold', 'NoOfHouseholdMembers', 'Wash']
    categorial_list = ['PopulationGroup', 'Status', 'RespondentSet', 'HeadOfHouseholdSex',
                       'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold']
    need_threshold = 4

    runExperiment(feature_list, categorial_list, need_threshold)


if __name__ == "__main__":
    main()
