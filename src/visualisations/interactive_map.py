"""
Simple interactive map created using Bokeh and Pandas-Bokeh, a higher-level library that
facilitates the usage of Bokeh, interfacing with dataframes.

Script for the Hack4Good program presentation on 20.05.2019
Author: V. de La Rochefoucauld
"""


import geopandas as gpd
import pandas as pd
from bokeh.plotting import show
from bokeh.palettes import brewer
import pandas_bokeh
from shapely.geometry import Polygon
import pickle
import yaml

# ------------------------- STEP 0: FIRST REMARKS... ---------------------------- #

"""
#If you need to recreate the geometrical files (wards_geometry and wards_latlon), you can use this script.

import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import wards_geometry as geo
geo.create_wards_geometry(save=True, returnme=False)
"""

# --------------------- SETUP I/O LOCATIONS AND READ DATA ----------------------- #

# Set output location for map html file. Goes to outputs folder per default.
pandas_bokeh.output_file('outputs/nigeria_map.html')

# Read the yaml file that contains all the necessary paths of raw and processed data.
files = yaml.load(open('./file_locations.yml','rb'))

# Read the geometry file containing the geographical shapes of the wards.
wards_geometry = pickle.load(open(files['wards_geometry'], 'rb'))

# Read in the predictions that were saved into a csv file.
predictions = pd.read_csv(files['predictions'])


# ------------------- MERGE GEOGRAPHICAL DATA AND ANALYSES-------------------------#

# Merge the average pins calculated before with the geographical data.
pins_and_wards = pd.merge(predictions, wards_geometry, how='left',on='Camp')

# Drop first column if present (unimportant remnant from csv import)
try:
    pins_and_wards.drop(columns=['Unnamed: 0','_parent_index'],axis=1,inplace=True)
except KeyError:
    pass

# ------------------------- SPECIFY COLUMNS TO PLOT ------------------------------- #

# We now distinguish between the PiN values and 'in need' index in the dataframe.
pin_cols = list(pins_and_wards.columns[2:10])
need_cols = [col + '_in_need' for col in pin_cols]
pred_cols = [col + '_pred' for col in pin_cols]


# ----------------------- AGGREGATE DATA FOR PLOTTING ------------------------------ #

# Aggregate the data by means of the camp for plotting
data_agg = pins_and_wards.copy()
aggregated = data_agg.groupby('Camp').mean()
aggregated['Camp'] = aggregated.index
aggregated.reset_index(drop=True, inplace=True)

# Merge it back to add the spatial information that got lost while grouping.
agg_geometry = pd.merge(aggregated, wards_geometry, how='left', on='Camp')


# ------------- SELECT WHAT TO PLOT & CONVERT TO GEODATAFRAME ----------------------#

# Read in what should be plotted (either predictions which are based on the 'in need'
# criterion or the actual PiN values which are not aggregated by the 'in need'
# criterion - PiN value > 4)

mode = files['vis_mode']

# if we are dealing with the prediction visualisation, then only take the relevant columns
if mode == 'prediction':

    # take only prediction columns
    pred_cols = [col for col in agg_geometry.columns if col not in pin_cols]
    pred_scores = agg_geometry[pred_cols].copy()

    # calculate percentages of people in need per camp
    for col in [col for col in pred_cols if col not in ['Camp', 'geometry']]:
        pred_scores.loc[:, col] = pred_scores.loc[:, col] * 100

    # convert it into a GeoDataFrame in order to be able to plot spatially.
    pnw_gpd = gpd.GeoDataFrame(pred_scores)

    # drop the overall columns, as we want to examine only the individual sectors
    pnw_gpd.drop(columns=['Overall_in_need', 'Overall_pred'], axis=1, inplace=True)

elif mode == 'pin_visualisation':

    # take only pin value columns
    pin_scores = agg_geometry[['Camp', 'geometry'] + pin_cols].copy()

    # convert it into a GeoDataFrame in order to be able to plot spatially.
    pnw_gpd = gpd.GeoDataFrame(pin_scores)

    # drop the overall column, as we want to examine only the individual sectors
    pnw_gpd.drop(columns=['Overall'], axis=1, inplace=True)


# ------------------------MAKE SURE LOCATION ENCODING MATCHES ----------------------------#


pnw_gpd.crs = {'init':'epsg:4326'}
pnw_gpd.loc[pnw_gpd['geometry']==0,'geometry'] = Polygon()
pnw_gpd.dropna(subset=['Camp'],inplace=True)


# --------------------------------- SETUP PLOT -------------------------------------------#

# Select a color palette
palette = brewer['YlOrRd'][5]

# Reverse color order so that dark red is highest obesity.
palette = palette[::-1]

# Create list of dropdown columns
dropdown_cols = [col for col in pnw_gpd.columns if not col in ['geometry','Camp']]

# Make map plot via pandas_bokeh library
p = pnw_gpd.plot_bokeh(
    figsize=(1300, 700),
    simplify_shapes=100,
    dropdown = dropdown_cols,
    colormap= palette,
    color = None,
    hovertool_columns=['Camp', dropdown_cols[0]],
    fill_alpha=0.3,
    show_figure=False
    )

# ----------------------------- MAKE CUSTOM ALTERATIONS -------------------------------------#

# In order to get the hover tool to update with only the relevant information according to the dropdown,
# you need to alter the CustomJS code for the javascript callback and also its arguments upon clicking
# the dropdown menu and selecting a new category.

# First, add the already existing HoverTool to the argument list of the Javascript callback
p.children[0].js_property_callbacks['change:value'][0].args['hover'] = p.children[1].hover[0]

# Then, alter the CustomJS codeblock to only show the selected category's value in the HoverTool.
p.children[0].js_property_callbacks['change:value'][0].code = """
        //Change selection of field for Colormapper for choropleth plot:
        geo_source.data["Colormap"] = geo_source.data[dropdown_widget.value];
        geo_source.change.emit();
        //Change label of Legend:
        legend.label["value"] = " " + dropdown_widget.value;
        var col = String(dropdown_widget.value);
        // Change value of hovertool:
        hover.tooltips[1][0] = "People in need %" ;
        hover.tooltips[1][1] = "@"+ col;
                    """

# Finally, adapt the text showing on the dropdown button to your taste.
p.children[0].label = 'Select Heatmap Criterion'

# ----------------------------------- SHOW PLOT --------------------------------------------#


# You can also just set show_figure to True above, but I like to do it down explicitly, especially after the changes.
show(p)
