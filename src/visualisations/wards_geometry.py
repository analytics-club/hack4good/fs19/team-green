"""
Processing scripts that create the geometry spatial location conversions for the visualisations.

Script for the Hack4Good program presentation on 20.05.2019
Author: V. de La Rochefoucauld
"""

import pandas as pd
from tqdm import tqdm
import numpy as np
from shapely.geometry import Point
import yaml
import pickle
import geopandas as gpd


def ward_matching(table, wards, files, save=True):
    """
    Function that matches all of the camps listed in the dataset with the geographically distinct wards.
    We take the more laborious approach by looping through all longitudes and latitudes of the different camps,
    in order to make sure we have no mismatches due to corrupted Camp names etc.
    :param table: cleaned, human readable dataset (dataframe)
    :param wards: geopandas file with spatial boundaries of nigerian wards
    :param files: file location dictionary
    :param save: boolean whether to save or not to save
    :return wards: matched dataframe with camp names and corresponding geospatial information
    """
    print('Matching wards and longitude latitudes.')
    print('This might take a while...')

    camps = table[['Camp','longitude','latitude']].copy()
    camps = camps.groupby('Camp').mean()
    wards['Camp'] = np.empty((len(wards),1),list)

    for idx, row in tqdm(camps.iterrows(), total = camps.shape[0]):
        for i,polygon in enumerate(wards.geometry):
            if Point(row['longitude'],row['latitude']).within(polygon):
                if wards.loc[i, 'Camp'] == None:
                    wards.loc[i, 'Camp'] = []
                wards.loc[i, 'Camp'].append(idx)
                continue
    if save:
        import pickle
        pickle.dump(wards, open(files['wards_lonlat'], "wb"))

    return wards


def merge_df_lists(df1, df2):
    """
    Very slow, but necessary modification to the normal pandas merge function. Merging will apparently not work
    when we have geospatial objects in the dataframe, so we opt for this workaround.
    :param df1: dataframe 1
    :param df2: dataframe 2
    :return: merged dataframe
    """
    for idx, row in tqdm(df1.iterrows(), total=df1.shape[0]):
        for i, wardrow in df2.iterrows():
                if not wardrow['Camp'] == None:
                    if row['Camp'] in wardrow['Camp']:
                        df1.loc[idx,'geometry'] = wardrow['geometry']
                        continue

    return df1


def create_wards_geometry(save=True, returnme=False):
    """
    Function that loads the cleaned dataset and shapefiles outlining Nigerian wards and either saves or returns it.
    :param save: boolean, if it is to be saved
    :param returnme: boolean, if instead of saving, it should be returned.
    :return:
    """
    # Load file locations, cleaned human readable dataset and shapefile containing nigeria ward boundaries.
    files = yaml.load(open('./file_locations.yml', 'rb'))
    table = pickle.load(open(files['human'], 'rb'))
    wards = gpd.read_file(files['nigeria_map_path_wards'])

    # Match the wards with the Camps
    wards = ward_matching(table, wards, files, save=True)

    # Eliminate double instances of wards
    wards_single = pd.DataFrame(data={'Camp': table['Camp'].unique(), 'geometry': 0})
    wards_geometry = merge_df_lists(wards_single, wards)

    # Save or return
    if save:
        pickle.dump(wards_geometry, open(files['wards_geometry'],"wb"))

    elif returnme:
        return wards_geometry

