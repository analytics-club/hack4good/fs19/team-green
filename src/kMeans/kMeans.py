import src.cleanup.cleanup as clp
from sklearn.cluster import KMeans
from yellowbrick.cluster import KElbowVisualizer
import matplotlib.pyplot as plt
import pandas as pd


#Load cleaned table
clean = clp.clean_table()

X = clean.human[['HouseholdID',u'longitude', u'latitude',u'Wash', u'Shelter', u'Livelihood', u'Recovery', u'Health',u'Nutrition', u'Education', u'Protection']]
X = X.dropna(how="any")
X = X.groupby('HouseholdID',as_index=False).mean()
y = X[['HouseholdID',u'longitude', u'latitude']]
X = X[[u'Wash', u'Shelter', u'Livelihood', u'Recovery', u'Health', u'Nutrition', u'Education', u'Protection']]

#Find best number of clusters
model = KMeans()
visualizer = KElbowVisualizer(model,k=(2,13))
visualizer.fit(X)
visualizer.poof()

# Looks bad, but let's give it a try with k=2 clusters
kmeans = KMeans(n_clusters=2)
kmeans.fit(X)
y_kmeans = kmeans.predict(X)

plt.scatter(y['longitude'],y['latitude'],c=y_kmeans,s=50)
plt.title("Clusters on a map")
plt.xlabel("Longitude")
plt.ylabel("Latitude")
plt.show()
