"""
Example Usage: python -m src.random_forest.random_forest
"""
import os
import time
import pickle
import numpy as np
import pandas as pd
import sklearn.ensemble
import sklearn.preprocessing
import sklearn.model_selection

import src.cleanup.cleanup as clp
import src.utils.experiment_report as experiment_report
import src.utils.evaluation_statistics as statistics


class RandomForest:
    def __init__(self):
        """Wrapper for the sklearn random forest model"""
        self.model = None

    def initialiseModel(self):
        """Initialises a new model. An already existing random forest model will be overwritten"""
        self.model = sklearn.ensemble.RandomForestClassifier(n_estimators=50,
                                                             n_jobs=-1,
                                                             verbose=0,
                                                             class_weight='balanced')

    def saveModel(self, time_name):
        """Saves the current model"""
        # forest_classifier.saveModel(report_writer.time)
        file_name = os.path.join('results', 'models', 'random_forest', 'weights_' + time_name)
        with open(file_name, 'wb') as f:
            pickle.dump(self.model, f)

    def loadModel(self, file_path):
        """Loads model specified by file_path. An already existing random forest model will be overwritten"""
        # forest_classifier.loadModel('results/models/random_forest/weights_20190430-224622')
        with open(file_path, 'rb') as f:
            self.model = pickle.load(f)


def loadData(feature_target_list, nr_target_variables, categorial_list, need_threshold):
    """Loads the data and encodes the categorial variable to an one hot embedding"""
    clean = clp.clean_table(reload_tables=False,
                            in_directory=os.path.join('data', 'raw'),
                            out_directory=os.path.join('data', 'processed'),
                            load_tif_files=True)
    clean_data_input = clean.table
    clean_data_input = clean_data_input.drop_duplicates(subset='HouseholdID', keep='first').dropna(subset=['RespondentSex'])
    clean_data_input = clean_data_input.dropna(subset=['literacy'])
    data_input = clean_data_input[feature_target_list]

    for key in categorial_list:
        data_input[key] = data_input[key].astype("category").cat.codes

    index_categorial_feature = [i for i in range(len(feature_target_list)) if feature_target_list[i] in categorial_list]
    one_hot_encoder = sklearn.preprocessing.OneHotEncoder(categorical_features=index_categorial_feature, sparse=False)
    one_hot_data = one_hot_encoder.fit_transform(data_input)

    for target in range(1, nr_target_variables+1):
        one_hot_data[one_hot_data[:, -target] < need_threshold, -target] = 0
        one_hot_data[one_hot_data[:, -target] >= need_threshold, -target] = 1

    return one_hot_data, clean_data_input


def runExperiment(X_train, X_test, y_train):
    """Fits and predicts a random forest model"""
    forest_classifier = RandomForest()
    forest_classifier.initialiseModel()

    start_train_time = time.time()
    forest_classifier.model.fit(X_train, y_train)
    training_time = time.time() - start_train_time
    print('Training Time: %s ' % "{0:.2f}".format(training_time))

    start_test_time = time.time()
    y_test_predicted = forest_classifier.model.predict(X_test)
    predict_time = time.time() - start_test_time

    # forest_classifier.saveModel(time_name=time.strftime("%Y%m%d-%H%M%S").time)

    return y_test_predicted, training_time, predict_time


def runKFTraining(X_train, y_train, list_features, target_variable):
    kf = sklearn.model_selection.KFold(n_splits=5, shuffle=True, random_state=1234)

    y_predicted_dataset = np.empty([0])
    y_ground_truth_dataset = np.empty([0])
    training_time_dataset = np.empty([0])
    predict_time_dataset = np.empty([0])

    for train_idx, val_idx in kf.split(X_train):
        output = runExperiment(X_train[train_idx, :], X_test=X_train[val_idx, :], y_train=y_train[train_idx])
        y_test_predicted, training_time, predict_time = output
        y_predicted_dataset = np.concatenate((y_predicted_dataset, y_test_predicted))
        y_ground_truth_dataset = np.concatenate((y_ground_truth_dataset, y_train[val_idx]))

    # ---- Create Writer for Storing a Summary of the experiment ----
    report_writer = experiment_report.ReportWriter(path_dir=os.path.join('results', 'models', 'random_forest'))
    report_writer.person_name = 'Nico'
    report_writer.workstation = 'Lenovo-ThinkPad-T460p'
    report_writer.model = 'Random Forest / KFold'

    # ---- Save Results ----
    report_writer.target_variable = target_variable
    report_writer.feature_list = list_features
    report_writer.nr_train_samples = X_train.shape[0]
    report_writer.nr_test_samples = X_train.shape[0]
    report_writer.time = time.strftime("%Y%m%d-%H%M%S")

    report_writer.time_training = np.mean(training_time_dataset)
    report_writer.time_predict = np.mean(predict_time_dataset)

    report_writer.saveReport(y_predicted=y_predicted_dataset, y_ground_truth=y_ground_truth_dataset)


def runTestExtperiment(X_train, X_test, y_train, y_test, list_features, target_variable):
    """Fits and predicts a random forest model"""
    # ---- Create Writer for Storing a Summary of the experiment ----
    report_writer = experiment_report.ReportWriter(path_dir=os.path.join('results', 'models', 'random_forest'))
    report_writer.person_name = 'Nico'
    report_writer.workstation = 'Lenovo-ThinkPad-T460p'
    report_writer.model = 'Random Forest'

    # ---- Fit Model ----
    forest_classifier = RandomForest()
    forest_classifier.initialiseModel()

    start_train_time = time.time()
    forest_classifier.model.fit(X_train, y_train)
    training_time = time.time() - start_train_time
    print('Training Time: %s ' % "{0:.2f}".format(training_time))

    start_test_time = time.time()
    y_test_predicted = forest_classifier.model.predict(X_test)
    predict_time = time.time() - start_test_time

    # ---- Save Results ----
    report_writer.target_variable = target_variable
    report_writer.feature_list = list_features
    report_writer.nr_train_samples = X_train.shape[0]
    report_writer.nr_test_samples = X_test.shape[0]
    report_writer.time = time.strftime("%Y%m%d-%H%M%S")

    report_writer.time_training = training_time
    report_writer.time_predict = predict_time

    report_writer.saveReport(y_predicted=y_test_predicted, y_ground_truth=y_test)
    # forest_classifier.saveModel(time_name=report_writer.time)


def runMultipleExeriments(feature_list, target_prediction, categorial_list, need_threshold, parameter_tuning=True):
    """Runs multiple experiments for the specified target_predictions and saves the result to a .csv file"""
    # ---- Load and Split Data ----
    data_input, _ = loadData(feature_list + target_prediction, len(target_prediction), categorial_list, need_threshold)
    random_state = 1234
    nr_target_predictions = len(target_prediction)
    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(data_input[:, :-nr_target_predictions],
                                                                                data_input[:, -nr_target_predictions:],
                                                                                test_size=0.2,
                                                                                random_state=random_state,
                                                                                shuffle=True)

    # Create dummy dataframe
    # columns = ['parent_index', 'Wash', 'Shelter', 'Livelihood', 'Recovery', 'Health', 'Nutrition', 'Education',
    #            'Protection', 'Wash_in_need', 'Shelter_in_need', 'Livelihood_in_need', 'Recovery_in_need',
    #            'Health_in_need', 'Nutrition_in_need', 'Education_in_need', 'Protection_in_need', 'Wash_pred',
    #            'Shelter_pred', 'Livelihood_pred', 'Recovery_pred', 'Health_pred', 'Nutrition_pred', 'Education_pred',
    #            'Protection_pred']
    # input_dataframe = pd.DataFrame(np.random.randint(0, 2, size=(100, len(columns))), columns=columns)
    # out_dir = 'results/final_evaluation'
    # statistics.createStats(input_dataframe, out_dir=out_dir, model='random_forest')

    if parameter_tuning:
        for target_idx in range(nr_target_predictions):
            runKFTraining(X_train, y_train[:, target_idx], feature_list, target_prediction[target_idx])

    else:
        for target_idx in range(nr_target_predictions):
            runTestExtperiment(X_train, X_test, y_train[:, target_idx], y_test[:, target_idx],
                               feature_list, target_prediction[target_idx])


def runPredictionKFold(feature_list, target_prediction, categorial_list, need_threshold):
    feature_list = feature_list
    data_input, clean_data = loadData(feature_list + target_prediction, len(target_prediction), categorial_list,
                                      need_threshold)
    parent_index = clean_data['_parent_index'].values

    ext_data_input = np.concatenate((parent_index[:, np.newaxis], data_input), axis=1)
    nr_target = len(target_prediction)

    output_prediction_df = pd.DataFrame(data=parent_index, columns=['_parent_index'])

    for target_idx, sector_name in enumerate(target_prediction):
        print('Predicting Sector %s' % sector_name)
        sector_df = fitKFold(ext_data_input[:, :-nr_target], ext_data_input[:, target_idx - nr_target], sector_name)
        output_prediction_df = output_prediction_df.merge(sector_df, how='outer', on='_parent_index')


def fitKFold(x_data, y_gt, sector_name):
    kf = sklearn.model_selection.KFold(n_splits=5, shuffle=True, random_state=1234)
    forest_classifier = RandomForest()
    forest_classifier.initialiseModel()
    pin_predictions = np.empty([0])
    parent_index = np.empty([0])
    nr_samples = x_data.shape[0]
    df = pd.DataFrame(index=range(nr_samples), columns=['_parent_index', sector_name + '_pred'])

    for train_idx, val_idx in kf.split(x_data):
        forest_classifier.model.fit(x_data[train_idx, 1:], y_gt[train_idx])
        output_predicted = forest_classifier.model.predict(x_data[val_idx, 1:])

        pin_predictions = np.concatenate((pin_predictions, output_predicted), axis=0)
        parent_index = np.concatenate((parent_index, x_data[val_idx, 0]))

    df[sector_name + '_pred'] = pin_predictions
    df['_parent_index'] = parent_index

    return df


def main():
    # Current Features:
    # ['PopulationGroup', 'Status', 'RespondentSex', 'HeadOfHouseholdSex',
    #        'HeadofHouseholdAge', 'HeadOfHouseholdMaritalStatus',
    #        'TribeOfHousehold', 'TribeOfHouseholdOtherText', 'NoOfHouseholdMembers',
    #        'HouseholdID', 'priority1', 'priority2', 'priority3', 'clusterName',
    #        'ShortID', 'Ward', 'Village', 'memberSex', 'memberAgeYears',
    #        'memberAgeMonths', 'Camp', 'longitude', 'latitude', 'HH',
    #        '_parent_index', 'Wash', 'Shelter', 'Food', 'Livelihood', 'Health',
    #        'Nutrition', 'Education', 'Protection', 'Overall', 'Ward_mean_Wash',
    #        'Ward_mean_Shelter', 'Ward_mean_Food', 'Ward_mean_Livelihood',
    #        'Ward_mean_Health', 'Ward_mean_Nutrition', 'Ward_mean_Education',
    #        'Ward_mean_Protection', 'Ward_mean_Overall', 'Village_mean_Wash',
    #        'Village_mean_Shelter', 'Village_mean_Food', 'Village_mean_Livelihood',
    #        'Village_mean_Health', 'Village_mean_Nutrition',
    #        'Village_mean_Education', 'Village_mean_Protection',
    #        'Village_mean_Overall', 'poverty', 'literacy', 'population']

    # feature_list = ['PopulationGroup', 'Status', 'RespondentSex', 'HeadOfHouseholdSex', 'HeadofHouseholdAge',
    #                 'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold', 'NoOfHouseholdMembers', 'HH']
    feature_list = ['PopulationGroup', 'Status', 'RespondentSex', 'HeadOfHouseholdSex', 'HeadofHouseholdAge',
                    'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold', 'NoOfHouseholdMembers']
    # feature_list = ['PopulationGroup', 'Status', 'RespondentSex', 'HeadOfHouseholdSex', 'HeadofHouseholdAge',
    #                 'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold', 'NoOfHouseholdMembers', 'poverty', 'population',
    #                 'literacy']

    target_prediction = ['Wash', 'Shelter', 'Food', 'Livelihood', 'Health', 'Nutrition', 'Education', 'Protection']

    categorial_list = ['PopulationGroup', 'Status', 'RespondentSex', 'HeadOfHouseholdSex',
                       'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold']
    need_threshold = 4

    runMultipleExeriments(feature_list, target_prediction, categorial_list, need_threshold, parameter_tuning=False)

    # runPredictionKFold(feature_list, target_prediction, categorial_list, need_threshold)


if __name__ == "__main__":
    main()
