
# coding: utf-8

# # Data Exploration & PiN calculation IMPACT Nigeria MSNA 
# 
# - Reading in the first geotif_dataset (clean geotif_dataset final)
# - Calculating the PiN based on Annex 6 (pg. 124) of Nigeria's reach report (see Excel PiN_calculation_details.xlsx for more details)



import numpy as np
import pandas as pd




def letter2num(letters, zbase=True):
    """A = 0, C = 2 and so on. Convert spreadsheet style column enumeration to a number """

    letters = letters.upper()
    res = 0
    weight = len(letters) - 1
    for i, c in enumerate(letters):
        res += (ord(c) - 64) * 26**(weight - i)
    if not zbase:
        return res
    return res - 1



def calculate_PiN(data=None, source_dir="../../data/raw"):
    # data = pd.ExcelFile('/Users/Vicky/Documents/Documents/Studium/IMPACT project/Data/reach_nga_msna_clean_dataset_final.xlsx')
    if data == None:
        data = pd.ExcelFile(source_dir+'/reach_nga_msna_clean_dataset_final.xlsx')
    sheets = data.sheet_names


    # Put each excel sheet in a separate dataframe

    readme = data.parse(0)
    hh_data = data.parse(1) # Households data
    ind_member_data = data.parse(2)
    ind_waterloop_data = data.parse(3)
    survey = data.parse(4)
    choices = data.parse(5)

    print('Workig on clean_hh_data sheet')
    # Delete rows with no parent_index at the bottom of the hh_data dataframe
    nan_rows = np.isnan(hh_data.loc[:, '_parent_index'])
    nan_rows = [i for i, val in enumerate(nan_rows) if val] # Get nans indexes
    print('Removing', len(nan_rows), 'rows because without _parent_index')
    hh_data.drop(index=nan_rows, inplace=True) # Delete rows with no _parent_index

    print('Number of households:', hh_data.shape[0])
    print('Number of features:  ', hh_data.shape[1])

    print('Workig on ind_hh_member_data sheet')
    # Delete rows with no parent_index at the bottom of the hh_data dataframe
    nan_rows = np.isnan(ind_member_data.loc[:, 'INDEX FOR HOUSEHOLD DATA'])
    nan_rows = [i for i, val in enumerate(nan_rows) if val] # Get nans indexes
    print('Removing', len(nan_rows), 'rows because without _parent_index')
    ind_member_data.drop(index=nan_rows, inplace=True) # Delete rows with no _parent_index

    print('Number of individuals:', ind_member_data.shape[0])
    print('Number of features:   ', ind_member_data.shape[1])


    # In[5]:


    # Calculate the PiN for each HH

    print ('=====================================')
    print ('Calculating the PiNs ...')

    # Look for the HHs, who refused to interview, for them there's no PiN to calculate
    # They'll have a nan in the PiN table
    HH_no_interview = (hh_data['Consent'] == 'No, refuses to interview')
    print('Number of HHs, who refused to interview:', sum(HH_no_interview))

    # ====================== WASH SECTOR =============================

    # HH is without access to any improved water source
    PiN_wash = 2. * ~ ((hh_data.iloc[:,letter2num('FG')].values == 'No') |
                    (hh_data.iloc[:,letter2num('FH')].values == 'No') |
                    (hh_data.iloc[:,letter2num('FI')].values == 'No') |
                    (hh_data.iloc[:,letter2num('FJ')].values == 'No') |
                    (hh_data.iloc[:,letter2num('FK')].values == 'No') |
                    (hh_data.iloc[:,letter2num('FL')].values == 'No'))

    # HH has access to less than 15 litres per person per day
    PiN_wash = ( PiN_wash + 
                3. * ( hh_data.iloc[:,letter2num('GB')].values < 15. ) )

    # HH is without access to a functioning latrine
    PiN_wash = ( PiN_wash +
                2. * ((hh_data.iloc[:,letter2num('IB')].values == 'No, defecate in the bush') |
                    (hh_data.iloc[:,letter2num('IB')].values == 'No, defecate in area designated by the community')) )

    # HH reports spending more than 30 minutes to collect water
    PiN_wash = ( PiN_wash +
                2. * ((hh_data.iloc[:,letter2num('HT')].values == 'From 30 minutes up to 1 hour ') |
                    (hh_data.iloc[:,letter2num('HT')].values == 'From 1 hour up to 2 hours') |
                    (hh_data.iloc[:,letter2num('HT')].values == 'Greater than 2 hours')) )

    # HH reports that there is no soap in the HH
    PiN_wash = ( PiN_wash +
                1. * (hh_data.iloc[:,letter2num('HW')].values == 'No') )

    PiN_wash[HH_no_interview] = np.nan
    print('PiN_wash', PiN_wash)
    print(np.nansum(PiN_wash))

    # ==================== SHELTER / NFI ================================

    # HH lives in an inadequate shelter
    PiN_shelter_NFI = 2. * ((hh_data.iloc[:,letter2num('UZ')].values == 'Makeshift (thatch house with collected materials)') |
                            (hh_data.iloc[:,letter2num('UZ')].values == 'Tent') |
                            (hh_data.iloc[:,letter2num('UZ')].values == 'Collective shelter (mosque, school or other public building)') |
                            (hh_data.iloc[:,letter2num('UZ')].values == 'No shelter / sleeps in the open space'))

    # HH shelter is damaged (partially or completely)
    PiN_shelter_NFI = ( PiN_shelter_NFI +
                    2. * (hh_data.iloc[:,letter2num('VL')].values == 1.) ) # '1' = 'Yes'

    # HH is at risk of eviction
    PiN_shelter_NFI = ( PiN_shelter_NFI +
                    2. * (hh_data.iloc[:,letter2num('VJ')].values == 1.) ) # '1' = 'Yes'

    # HH owns less than half of items from basic NFI kit
    # Number of NFI items in the questionnaire
    col_idxs = idxs = [ letter2num(col) for col in ['WA','WB','WC','WD','WE','WF','WG','WH',
                                                'WM','WN',
                                                'WP','WQ'] ]
    print ('#NFI items considered:', len(col_idxs))
    PiN_shelter_NFI = ( PiN_shelter_NFI +
                    2. * (np.sum( hh_data.iloc[:,col_idxs].values == 'Yes', axis=1 ) < 7.) )

    # HH shares shelter with 2 or more families (i.e., shelter with at least 2 families!)
    PiN_shelter_NFI = ( PiN_shelter_NFI +
                    2. * (hh_data.iloc[:,letter2num('VK')].values > 1.1) )

    PiN_shelter_NFI[HH_no_interview] = np.nan
    print('PiN_shelter_NFI', PiN_shelter_NFI)
    print(np.nansum(PiN_shelter_NFI))

    # ===================== FOOD SECURETY AND LIVELIHOODS ====================

    # HH has a borderline / poor FCS (food consumption score)
    # Calculate the FCS
    FCS = (2.  * hh_data.iloc[:,letter2num('ND')].values +
        3.  * hh_data.iloc[:,letter2num('NE')].values +
        1.  * hh_data.iloc[:,letter2num('NF')].values +
        1.  * hh_data.iloc[:,letter2num('NG')].values +
        4.  * hh_data.iloc[:,letter2num('NH')].values +
        4.  * hh_data.iloc[:,letter2num('NI')].values +
        0.5 * hh_data.iloc[:,letter2num('NJ')].values +
        0.5 * hh_data.iloc[:,letter2num('NK')].values )

    PiN_food = (3. * (FCS<21.) +
                    2. * ((FCS>21.) & (FCS<35.)) )

    # HH has a high use on reduced Coping Strategy Index
    # Caluclate rCSI
    rCSI = (1.  * hh_data.iloc[:,letter2num('NM')].values +
            2.  * hh_data.iloc[:,letter2num('NN')].values +
            1.  * hh_data.iloc[:,letter2num('NO')].values +
            3.  * hh_data.iloc[:,letter2num('NP')].values +
            1.  * hh_data.iloc[:,letter2num('NQ')].values )

    PiN_food = ( PiN_food +
                    3. * (rCSI>9.99) )

    # HH reports using unsafe/unsustainable fuel for cooking
    PiN_food = ( PiN_food +
                    0.33 * ((hh_data.iloc[:,letter2num('OY')].values == 'Firewood') |
                            (hh_data.iloc[:,letter2num('OY')].values == 'Animal dung') |
                            (hh_data.iloc[:,letter2num('OY')].values == 'Agricultural waste / crop residue')) )

    # HH reports using unsafe/unsustainable fuel for lighting
    PiN_food = ( PiN_food +
                    0.33 * ((hh_data.iloc[:,letter2num('PA')].values == 'Firewood') |
                            (hh_data.iloc[:,letter2num('PA')].values == 'None')) )

    # HH reports using unsafe/unsustainable method for cooking
    PiN_food = ( PiN_food +
                    0.33 * (hh_data.iloc[:,letter2num('PS')].values == 'Three-stone fire') )

    # HH reports unsafe/ unsustainable means of obtaining primary fuel source
    PiN_food = ( PiN_food +
                    0.33 * ((hh_data.iloc[:,letter2num('PC')].values == 'Collect directly from outside the community') |
                            (hh_data.iloc[:,letter2num('PC')].values == 'Trade goods or items for fuel') |
                            (hh_data.iloc[:,letter2num('PC')].values == 'From NGO aid  / assistance')) )

    # HH reports resorting to negative fuel coping strategies
    PiN_food = ( PiN_food +
                    2. * ((hh_data.iloc[:,letter2num('PX')].values == 'Yes') |
                            (hh_data.iloc[:,letter2num('QH')].values == 'Yes')) )

    PiN_food = ( PiN_food +
                    0.33 * ((hh_data.iloc[:,letter2num('PZ')].values == 'Yes') |
                            (hh_data.iloc[:,letter2num('QD')].values == 'Yes') |
                            (hh_data.iloc[:,letter2num('QF')].values == 'Yes') |
                            (hh_data.iloc[:,letter2num('QG')].values == 'Yes') ) )

    # HH reports no access to markets

    PiN_food = ( PiN_food +
                       1. * (hh_data.iloc[:,letter2num('OF')].values == 2.) ) # '2' = 'No'

    # HH reports market-related barriers to accessing food items
    PiN_food = ( PiN_food +
                    1. * (hh_data.iloc[:,letter2num('OG')].values == 'No') )

    # HH was reportedly not able to plant / harvest last dry season
    PiN_food = ( PiN_food +
                    0.5 * ((hh_data.iloc[:,letter2num('UB')].values == "Didn't plant or harvest") |
                            (hh_data.iloc[:,letter2num('UB')].values == 'Planted but did not harvest anything')) )

    # HH reports not planning to cultivate this rainy season
    PiN_food = ( PiN_food +
                    0.5 * (hh_data.iloc[:,letter2num('UC')].values == 'No, will not plant or harvest this rainy season') )

    # HH reports not accessing: amount of land needed / land at all
    PiN_food = ( PiN_food +
                    0.5 * (hh_data.iloc[:,letter2num('UG')].values == 'No, did not access any land') )

    PiN_food = ( PiN_food +
                    0.25 * (hh_data.iloc[:,letter2num('UG')].values == 'Yes, but did not access amount of land needed') )

    # HH reports not accessing: amount of water needed / water at all
    PiN_food = ( PiN_food +
                    0.5 * (hh_data.iloc[:,letter2num('UJ')].values == 'No, did not access any water') )

    PiN_food = ( PiN_food +
                    0.25 * (hh_data.iloc[:,letter2num('UJ')].values == 'Yes, but did not access amount of water needed') )

    PiN_food[HH_no_interview] = np.nan
    print('PiN_food', PiN_food)
    print(np.nansum(PiN_food))

    # =================== EARLY RECOVERY AND LIVELIHOODS ============================

    # HH income has decreased in the previous 3 months
    PiN_livelihood = 2. * (hh_data.iloc[:,letter2num('TW')].values == 'Decrease')

    # HH reports being in debt
    PiN_livelihood = ( PiN_livelihood +
                2. * (hh_data.iloc[:,letter2num('TX')].values == 1.) ) # '1' = 'Yes'

    # HH reports using “crisis” or “emergency” coping strategies
    PiN_livelihood = ( PiN_livelihood +
                3. * ( (hh_data.iloc[:,letter2num('RO'):(letter2num('RX')+1)].values == 'Yes').any(axis=1) ) )
                    
    # HH reports no access to physical cash
    PiN_livelihood = ( PiN_livelihood +
                3. * (hh_data.iloc[:,letter2num('TY')].values == 'No access to cash') )

    PiN_livelihood[HH_no_interview] = np.nan
    print('PiN_livelihood', PiN_livelihood)
    print(np.nansum(PiN_livelihood))

    # ====================== NUTRITION & individual-related EDUCATION/HEALTH =========================

    # HH has a moderately or severely malnourished child
    # Household has children that are not currently attending any formal or informal school
    # Household has children that have never attended any formal school
    # HH has child/ren without any immunization

    PiN_nutrition             = np.zeros(PiN_livelihood.shape)
    PiN_school_attending      = np.zeros(PiN_livelihood.shape)
    PiN_school_never_attended = np.zeros(PiN_livelihood.shape)
    PiN_vaccination           = np.zeros(PiN_livelihood.shape)

    # Fetch info about the individual memeber in the HH
    for iCol in range(len(PiN_nutrition)) :
    #     print('pos:', [iCol,letter2num('AIG')])
        parent_index = hh_data.iloc[iCol,letter2num('AIG')] # Get the _parent_index
        ind_idxs = ind_member_data['INDEX FOR HOUSEHOLD DATA'] == parent_index
        
        # Nutrition - MUAC
        muacs = ind_member_data.loc[ind_idxs, 'Child MUAC Measurement (mm)'].values
        if (len(muacs)) :
            PiN_nutrition[iCol] = np.nanmin(muacs)
            
        # Education - School
        school_formal = ind_member_data.loc[ind_idxs, 'What is the current FORMAL school attendance status of ?'].values
        school_inf    = ind_member_data.loc[ind_idxs, 'What is the current INFORMAL school attendance status of ?'].values
        
        # The .any() is there because there are multiple individuals in a single HH
        if((((school_formal == 'Never attended any formal school') | 
            (school_formal == 'Did not attend any formal school this year') | 
            (school_formal == 'Dropped out this year') ) &
            ((school_inf    == 'Never attended any non-formal education') | 
            (school_inf    == 'Did not attend non-formal education this year') | 
            (school_inf    == 'Dropped out of non-formal education this year') ) ).any() ) :
            PiN_school_attending[iCol] = 1.
            
        if(((school_formal == 'Never attended any formal school') &
            (school_inf    == 'Never attended any non-formal education') ).any() ):
            PiN_school_never_attended[iCol] = 1.
            
        # Health - vaccinations
        vac_measles = ind_member_data.loc[ind_idxs, 'Has  received any measles vaccine?'].values
        vac_PENTA   = ind_member_data.loc[ind_idxs, 'Has  received any PENTA vaccine shots?'].values
        vac_polio   = ind_member_data.loc[ind_idxs, 'Has  received any polio (OPV) vaccine?'].values
        
        if (len(vac_measles)) :
            PiN_vaccination[iCol] = ((vac_measles == 'No') &
                                    (vac_PENTA   == 'No') &
                                    (vac_polio   == 'No')).any()
            
    PiN_nutrition = 10. * (PiN_nutrition < 125.)

    PiN_nutrition[HH_no_interview] = np.nan
    print('PiN_nutrition', PiN_nutrition)
    print(np.nansum(PiN_nutrition))

    # =================== HEALTH ============================

    # HH reports at least 1 barrier to accessing health services
    PiN_health = 2. * (hh_data.iloc[:,letter2num('KW')].values == 'No')

    # HH has child/ren without any immunization
    PiN_health = (PiN_health + 
                2. * PiN_vaccination)

    # HH member had illness in the previous 2 weeks
    PiN_health = ( PiN_health +
                2. * (hh_data.iloc[:,letter2num('JK')].values == 1.) ) # '1' = 'Yes'

    # HH reports being too far from nearest health facility
    PiN_health = ( PiN_health +
                2. * ((hh_data.iloc[:,letter2num('KT')].values == 'Within 2-5km') |
                        (hh_data.iloc[:,letter2num('KT')].values == 'More than 5km')) )

    # HH experiences childbirth without skilled birth attendant
    PiN_health = ( PiN_health +
                2. * ((hh_data.iloc[:,letter2num('LY')].values == 'Other health care worker (health volunteer, CHEW)') |
                        (hh_data.iloc[:,letter2num('LY')].values == 'Traditional birth attendant') |
                        (hh_data.iloc[:,letter2num('LY')].values == 'Other women in the community') |
                        (hh_data.iloc[:,letter2num('LY')].values == 'No support') |
                        (hh_data.iloc[:,letter2num('LY')].values == 'Other')) )

    PiN_health[HH_no_interview] = np.nan
    print('PiN_health', PiN_health)
    print(np.nansum(PiN_health))

    # ====================== EDUCATION ===============================

    # Household has children that are not currently attending any formal or informal school
    PiN_education = 3. * PiN_school_attending

    # Household has children that have never attended any formal school
    PiN_education = (PiN_education + 
                    3. * PiN_school_never_attended)

    # Household reports any barrier in accessing schools
    PiN_education = (PiN_education +
                    2. * (hh_data.iloc[:,letter2num('XE')].values == 'No') )
                    
    # Household reports not owning school supplies
    PiN_education = (PiN_education + 
                    2. * ((hh_data.iloc[:,letter2num('WT')].values == 'Yes') |
                        (hh_data.iloc[:,letter2num('WU')].values == 'Yes') |
                        (hh_data.iloc[:,letter2num('WV')].values == 'Yes')) )

    PiN_education[HH_no_interview] = np.nan
    print('PiN_education', PiN_education)
    print(np.nansum(PiN_education))

    # ========================= PROTECTION ============================

    # HH is located in ward where explosive incidents were reported

    # HH has experienced a security incident in previous 3 months
    PiN_protection = 2. * (hh_data.iloc[:,letter2num('YH')].values == 1.) # '1' = 'Yes'

    # HH adult members do not have any legal documentation
    PiN_protection = ( PiN_protection +
                    2. * (hh_data.iloc[:,letter2num('YX')].values == 0.) )

    # HH experiences movement restrictions
    PiN_protection = ( PiN_protection +
                    2. * ((hh_data.iloc[:,letter2num('ZX')].values == 'Yes, but only during the evening and nighttime') |
                            (hh_data.iloc[:,letter2num('ZX')].values == 'Yes, but only if there are multiple household members') |
                            (hh_data.iloc[:,letter2num('ZX')].values == 'Yes, complete movement restrictions') |
                            (hh_data.iloc[:,letter2num('ZX')].values == 'Yes, from 5-10km outside of the camp or community')) )

    # HH has members that are missing / detained
    PiN_protection = ( PiN_protection +
                    2. * (hh_data.iloc[:,letter2num('AAB')].values == 'Yes') )

    PiN_protection[HH_no_interview] = np.nan
    print('PiN_protection', PiN_protection)
    print(np.nansum(PiN_protection))

    print ('=====================================')

    # =================================================
    # Merge the PiN from all sectors in a padnas dataframe
    # df_PiN structure: [_parent_index, PiNs_by_sector, overall_PiN]

    PiN_array = np.stack((hh_data.loc[:, '_parent_index'],
                        PiN_wash,
                        PiN_shelter_NFI,
                        PiN_food,
                        PiN_livelihood,
                        PiN_health,
                        PiN_nutrition,
                        PiN_education,
                        PiN_protection), axis=1)

    df_PiN = pd.DataFrame( PiN_array,
                        columns=['_parent_index','Wash', 'Shelter', 'Food', 'Livelihood', 'Health',
                                'Nutrition', 'Education', 'Protection']  )

    # Calculate the total PiN, if some sector-specific PiN are nan the result will be nan
    df_PiN['Overall'] = df_PiN.iloc[:,1:].sum(axis=1,skipna=False)


    return df_PiN



