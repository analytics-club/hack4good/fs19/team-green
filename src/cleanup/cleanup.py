

import numpy as np
import pandas as pd
import os

import src.cleanup.PiN.PiN_calculation as PiN_calculation
# import PiN.PiN_calculation as PiN_calculation


class clean_table:
    """Class which generates cleaned up table from household questionnaire xls-files and geotif_dataset
    Common usage:
    
    import cleanup as clp
    clean = clp.clean_table()   #generates a new cleaned up table class instance
    clean.generate_PiN()            #calculate PiN from scratch (only necessary if PiN-data have changed)
    clean.table                     #machine readable table
    clean.human                     #human readable table
    clean.translations              #table matching human readible strings to machine readible strings
    clean.one_hot                   #one-hot encoded table
    
    
    """
    def __init__(self, reload_tables=False, in_directory='data/raw', out_directory='data/processed',
                 load_tif_files=False):
        """Initialize class
        Loads the raw data and brings them into a neat form. To improve speed, final tables are loaded from pickle files (and changes in the xls-data files are ignored) UNLESS "reload_tables" is set to True.
        
        Arguments:
        reload_tables: Set to True if original xls files changed. This need to be done only once.
        in_directory: Folder where raw data xls files are located.
        out_directory: Directory for pickle tables to quickly store and load tables.
        
        """
        self.geotif_dataset = None
        self.band = None

        if reload_tables or (not os.path.isfile(out_directory+"/clean.pickle")):
            if not os.path.isfile(out_directory+"/clean.pickle"):
                print("Did not find cleaned pickle-Table at "+out_directory)
            self.generate(in_directory, out_directory, load_tif_files)
        else:
            self.table = pd.read_pickle(out_directory+"/clean.pickle")
            self.human = pd.read_pickle(out_directory+"/human.pickle")
            self.translations = pd.read_pickle(out_directory+"/translations.pickle")
            self.raw = pd.read_pickle(out_directory+"/raw.pickle")
            self.one_hot = pd.read_pickle(out_directory+"/one_hot.pickle")

    def letter2num(self, letters, zbase=True):
        """A = 0, C = 2 and so on. Convert spreadsheet style column enumeration to a number."""
        letters = letters.upper()
        res = 0
        weight = len(letters) - 1
        for i, c in enumerate(letters):
            res += (ord(c) - 64) * 26**(weight - i)
        if not zbase:
            return res
        return res - 1

    def colnum_string(self, n):
        n = n+1
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        return string

    def machine_encoding(self, table, columns, unordered=True):
        X = table.copy()
        translation = [] 
        for i in columns:
            col = X.keys()[i]
            k = 0
            ids = X.iloc[:, i].unique()
            if not unordered:
                ids = ids.sort_values()
            for j in ids:
                if pd.isnull(j):
                    X.loc[X[col] == j, col] = np.nan
                elif unordered:
                    X.loc[X[col] == j, col] = self.colnum_string(k)
                    translation.append({'column': table.keys()[i], 'human': j, 'machine': self.colnum_string(k)})
                else:
                    X.loc[X[col] == j, col] = k
                    translation.append({'column': table.keys()[i], 'human': j, 'machine': k})
                k = k+1
        return pd.DataFrame(translation), X
    
    def generate(self, in_directory, out_directory, load_tif_files):
        """Helper function which actually generates cleaned data tables from scratch"""
        print("Generating from scratch and saving to "+out_directory+" - this may take some minutes..")
        print("Reading in Variables...")
        if not os.path.isfile(in_directory+"/hh_data.pickle"):
            data = pd.ExcelFile(in_directory+'/reach_nga_msna_clean_dataset_final.xlsx')
            sheets = data.sheet_names

            readme = data.parse(0)
            hh_data = data.parse(1)
            ind_member_data = data.parse(2)
            ind_waterloop_data = data.parse(3)
            survey = data.parse(4)
            choices = data.parse(5)
            readme.to_pickle(in_directory+"/readme.pickle")
            hh_data.to_pickle(in_directory+"/hh_data.pickle")
            ind_member_data.to_pickle(in_directory+"/ind_member_data.pickle")
            ind_waterloop_data.to_pickle(in_directory+"/ind_waterloop_data.pickle")
            survey.to_pickle(in_directory+"/survey.pickle")
            choices.to_pickle(in_directory+"/choices.pickle")
            
        else:
            readme = pd.read_pickle(in_directory+"/readme.pickle")
            hh_data = pd.read_pickle(in_directory+"/hh_data.pickle")
            ind_member_data = pd.read_pickle(in_directory+"/ind_member_data.pickle")
            ind_waterloop_data = pd.read_pickle(in_directory+"/ind_waterloop_data.pickle")
            survey = pd.read_pickle(in_directory+"/survey.pickle")
            choices = pd.read_pickle(in_directory+"/hh_data.pickle")

        clean = hh_data.iloc[:, [self.letter2num('J'), self.letter2num('K'), self.letter2num('P'), self.letter2num('Q'),
                                 self.letter2num('R'), self.letter2num('S'), self.letter2num('BC'),
                                 self.letter2num('BD'), self.letter2num('BE'), self.letter2num('AIF'),
                                 self.letter2num('AHY'), self.letter2num('AIA'), self.letter2num('AIC'),
                                 self.letter2num('G'), self.letter2num('AIG'), self.letter2num('E'),
                                 self.letter2num('F')]]
        clean.columns = ['PopulationGroup', 'Status', 'RespondentSex', 'HeadOfHouseholdSex', 'HeadofHouseholdAge',
                         'HeadOfHouseholdMaritalStatus', 'TribeOfHousehold', 'TribeOfHouseholdOtherText',
                         'NoOfHouseholdMembers', 'HouseholdID', 'priority1', 'priority2', 'priority3', 'clusterName',
                         'ShortID', 'Ward', 'Village']
        clean = clean[pd.notnull(clean['HouseholdID'])]
        
        clean_member = ind_member_data.iloc[:, [self.letter2num('I'), self.letter2num('L'), self.letter2num('M'),
                                                self.letter2num('N')]]
        clean_member = clean_member[pd.notnull(clean_member['UNIQUE RECORD/HOUSEHOLD IDENTIFIER'])]
        clean_member.columns = ['HouseholdID', 'memberSex', 'memberAgeYears', 'memberAgeMonths']

        initial = pd.ExcelFile(in_directory+'/reach_nga_msna_initial_sample.xlsx').parse(0)
        initial = initial.iloc[:, [4, 10, 11, 9]]
        initial.columns = ['Camp', 'longitude', 'latitude', 'HH']
        initial = initial.drop(initial.index[0])
        #Make Camp-names more uniform: remove blank spaces and underscores and make them lower case
        initial['Camp'] = initial['Camp'].apply(lambda x: x.lower().replace(" ", "").replace("_", ""))
        initial['longitude']=pd.to_numeric(initial['longitude'])
        initial['latitude']=pd.to_numeric(initial['latitude'])
        initial['HH']=pd.to_numeric(initial['HH'])
        initial = initial.groupby('Camp', as_index=False).median()

        total = pd.merge(clean, clean_member, left_on='HouseholdID', right_on='HouseholdID', how='outer')
        total = pd.merge(total, initial, left_on='clusterName', right_on='Camp', how='left')
        human = total.copy()

        print("Generating PiN...")
        data = pd.ExcelFile(in_directory+'/reach_nga_msna_clean_dataset_final.xlsx')
        df_PiN = PiN_calculation.calculate_PiN(data)        
        human = pd.merge(human, df_PiN, left_on='ShortID', right_on='_parent_index', how='outer')

        print("Calculating Wards and Village averages...")
        human = self.geographical_PiN(human)
        self.human = human

        print("Generating Machine Encoding...")
        table = self.machine_encoding(human.copy(), [0, 1, 2, 3, 5, 6, 7, 9, 10, 11, 12, 13, 15, 18])
        translations = table[0]
        table = table[1]
        self.table = table

        print("Generating One-Hot Encoding...")
        self.generate_one_hot([0, 1, 2, 3, 5, 6, 7, 10, 11, 12, 13, 15, 18])

        if load_tif_files:
            print("Loading External Data...")
            self.load_external_variables(in_directory)

        self.table.to_pickle(out_directory+"/clean.pickle")
        human.to_pickle(out_directory+"/human.pickle")
        translations.to_pickle(out_directory+"/translations.pickle")
        hh_data.to_pickle(out_directory+"/raw.pickle")
        self.one_hot.to_pickle(out_directory+"/one_hot.pickle")
    
        print("Pickle files saved.")
        self.human = human
        self.translations = translations
        self.raw = hh_data
            
    def generate_one_hot(self, columns):
        """Helper function: Generates one-hot encoded table"""
        table = self.table.copy()
        xcols = 0
        for i in columns:
            xcols = xcols + table.iloc[:, i].unique().shape[0]
        X = np.zeros(shape=(table.shape[0], xcols))
        colnames = [None]*xcols
        current_col = 0
        
        for i in columns:
            ids = table.iloc[:, i].unique()
            col = table.keys()[i]
            for j in ids:
                colnames[current_col] = col+'_._'+str(j)
                current_col = current_col + 1
        X = pd.DataFrame(X)
        X.columns = colnames

        for i in columns:
            ids = table.iloc[:, i].unique()
            col = table.keys()[i]
            for j in ids:
                X.loc[table[col] == j, col+'_._'+str(j)] = 1

        table.drop(table.columns[columns], axis=1, inplace=True)
        self.one_hot = pd.concat([table, X])
        return 
    
    def geographical_PiN(self, human):
        """Averages PiN over Wards and Villages"""
        colnames_ward = ['Ward', 'Wash', 'Shelter', 'Food', 'Livelihood', 'Health', 'Nutrition', 'Education',
                         'Protection', 'Overall']

        wards = self.geo_helper(human, colnames_ward)
        colnames_ward = "Ward_mean_"+wards.columns
        colnames_ward = colnames_ward.values
        colnames_ward[0] = "Ward"
        wards.columns = colnames_ward
        
        colnames_village = ['Village', 'Wash', 'Shelter', 'Food', 'Livelihood', 'Health', 'Nutrition', 'Education',
                            'Protection', 'Overall']

        village = self.geo_helper(human, colnames_village)
        colnames_village = "Village_mean_"+village.columns
        colnames_village = colnames_village.values
        colnames_village[0] = "Village"
        village.columns = colnames_village      
        
        human = pd.merge(human, wards, left_on="Ward", right_on="Ward", how="outer")
        human = pd.merge(human, village, left_on="Village", right_on="Village", how="outer")
        
        return human

    def geo_helper(self, table, colnames):
        """Internal helper function"""
        return table[colnames].groupby(colnames[0], as_index=False).mean()

    def load_external_variables(self, in_dir):
        """Loads additional data from geotif files"""
        dir_path = os.path.join(os.path.dirname(in_dir), 'external')
        feature_to_include = ['poverty', 'literacy', 'population']

        hh_data = self.table.drop_duplicates(subset='HouseholdID', keep='first').dropna(subset=['longitude'])

        for feature in feature_to_include:
            hh_data = self.add_features(feature, dir_path, hh_data)

        add_features = feature_to_include + ['HouseholdID']
        self.table = pd.merge(left=self.table, right=hh_data[add_features], on="HouseholdID", how="left")

    def add_features(self, feature, dir_path, hh_data):
        """Loads additional data specified by the feature name"""
        import rasterio

        path_dir = os.path.join(dir_path, feature)
        path_file = os.path.join(path_dir, os.listdir(path_dir)[0])

        self.geotif_dataset = rasterio.open(path_file)
        self.band = self.geotif_dataset.read(1)

        hh_data[str(feature)] = hh_data.apply(lambda row: self.extract_value(row['longitude'], row['latitude']),
                                              axis=1)

        return hh_data

    def extract_value(self, long, lat):
        row, col = self.geotif_dataset.index(long, lat)

        return self.band[row, col]
