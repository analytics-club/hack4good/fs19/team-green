import os
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


class ReportWriter:
    def __init__(self, path_dir):
        """Wrapper for saving experiment summary to a .pdf file"""
        self.time = None
        self.model = None
        self.test_score = None
        self.person_name = None
        self.workstation = None
        self.feature_list = None
        self.time_predict = None
        self.time_training = None
        self.target_variable = None
        self.nr_test_samples = None
        self.nr_train_samples = None

        self.path_dir = path_dir

    def saveReport(self, y_predicted, y_ground_truth):
        """Save the report based on the predicte and grount truth labels as well as the input classes"""
        self.test_score = sklearn.metrics.accuracy_score(y_ground_truth, y_predicted)

        if not os.path.isdir(self.path_dir):
            os.makedirs(self.path_dir)

        pdf_file_name = os.path.join(self.path_dir, 'summary_report_' + self.target_variable + '_' + self.time + '.pdf')
        with PdfPages(pdf_file_name) as pdf:
            self.plotInformation(pdf)

            classifcation_report = sklearn.metrics.classification_report(y_ground_truth, y_predicted)
            self.plotClassifcationReport(classifcation_report, pdf)

            confusion_matrix = sklearn.metrics.confusion_matrix(y_ground_truth, y_predicted)
            self.plotConfusionMatrix(confusion_matrix, ['0', '1', '2', '3', '4', '5', '6', '7', '8'], pdf)

    def plotInformation(self, pdf):
        """Creates a tabel with general infomration about the experiment"""
        fig, ax = plt.subplots(figsize=(7, 3))
        ax.axis('tight')
        ax.set_ylim([-0.04, 0.2])
        ax.axis('off')
        table_data = self.createTableData()
        table = ax.table(cellText=table_data, cellLoc='left', loc='center')

        feature_txt = 'Features: \n' + ', '.join(self.feature_list)
        ax.text(0, -0.04, feature_txt, size=8, wrap=True, horizontalalignment='center', verticalalignment='center')
        pdf.savefig()
        plt.close()

    def createTableData(self):
        """Generates a 2D list for the table"""
        table_data = [['Experiment Information', ''],
                      ['Name', self.person_name],
                      ['Time', self.time],
                      ['Model', self.model],
                      ['Workstation', self.workstation],
                      [' ', ' '],
                      ['Score', '{0:.2f}'.format(self.test_score)],
                      ['Number of Train Samples', self.nr_train_samples],
                      ['Number of Test Samples', self.nr_test_samples],
                      ['Training Time', '{0:.2f}'.format(self.time_training)],
                      ['Prediction Time', '{0:.2f}'.format(self.time_predict)]]

        return table_data

    def plotClassifcationReport(self, classifcation_report, pdf):
        """Creates a table with precision, recall and f1-score"""
        formatted_report = self.formatClassifcationReport(classifcation_report)
        fig, ax = plt.subplots(figsize=(7, 3))
        ax.axis('tight')
        ax.set_ylim([-0.04, 0.2])
        ax.axis('off')
        table = ax.table(cellText=formatted_report, cellLoc='left', loc='center')

        pdf.savefig()
        plt.close()

    def formatClassifcationReport(self, classifcation_report):
        """Extracts a 2D file from the classification_report string"""
        formatted_report = []
        for row in classifcation_report.split('\n'):
            words = row.split()
            formatted_report.append(words)

        formatted_report[0] = ['Classes'] + formatted_report[0]
        formatted_report[1] = [' '] * 5
        formatted_report[-5] = [' ']*5

        for index_averages in range(-4, -1):
            formatted_report[index_averages].pop(1)
            formatted_report[index_averages][0] = formatted_report[index_averages][0] + ' average'

        return formatted_report[:-1]

    def plotConfusionMatrix(self, confusion_matrix, classes, pdf, cmap=plt.cm.Blues):
        """
        Plots a confusion Matrix.

        Taken from "https://scikit-learn.org/stable/auto_examples/model_selection/
        plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py"
        """
        confusion_matrix = confusion_matrix.astype('float') / confusion_matrix.sum(axis=1)[:, np.newaxis]
        fig, ax = plt.subplots()
        im = ax.imshow(confusion_matrix, interpolation='nearest', cmap=cmap)
        ax.figure.colorbar(im, ax=ax)

        ax.set(xticks=np.arange(confusion_matrix.shape[1]),
               yticks=np.arange(confusion_matrix.shape[0]),
               xticklabels=classes, yticklabels=classes,
               ylabel='True label',
               xlabel='Predicted label')

        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
        thresh = confusion_matrix.max() / 2.
        for i in range(confusion_matrix.shape[0]):
            for j in range(confusion_matrix.shape[1]):
                ax.text(j, i, format(confusion_matrix[i, j], '.2f'),
                        ha="center", va="center",
                        color="white" if confusion_matrix[i, j] > thresh else "black")
        fig.tight_layout()

        pdf.savefig()
        plt.close()

        return ax
