import os
import time
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


def createStats(input_dataframe, model, out_dir='results/final_evaluation'):
    pin_sectors = ['Wash', 'Shelter', 'Livelihood', 'Recovery', 'Health', 'Nutrition', 'Education', 'Protection']
    low_threshold = 2
    high_threshold = 6

    pdf_file_name = os.path.join(out_dir, model + '_' + time.strftime("%Y%m%d-%H%M%S"))
    pdf = PdfPages(pdf_file_name)

    for sector in pin_sectors:
        print('Calculating score for ' + sector)
        key_in_need = sector + '_in_need'
        key_pred = sector + '_pred'
        data_gt = input_dataframe[key_in_need]
        data_pred = input_dataframe[key_pred]

        plotConfusionF1(data_gt, data_pred, sector, pdf)

        plotSeverityStatistics(input_dataframe[[sector, key_in_need, key_pred]], low_threshold, high_threshold, sector,
                               pdf)

    pdf.close()


def plotConfusionF1(data_gt, data_pred, sector, pdf):
    fig, ax = plt.subplots(figsize=(6.5, 1))
    f1_score = sklearn.metrics.accuracy_score(y_true=data_gt, y_pred=data_pred)
    table_data = [['Sector', sector],
                  ['F1-Score', f1_score]]
    ax.table(cellText=table_data, cellLoc='left', loc='center')
    ax.axis('off')
    pdf.savefig()
    plt.close()

    confusion_matrix = sklearn.metrics.confusion_matrix(y_true=data_gt, y_pred=data_pred)
    plotConfusionMatrix(confusion_matrix, ['Not In Need', 'In Need'], pdf)


def plotSeverityStatistics(sector_data_frame, low_threshold, high_threshold, sector, pdf):
    data_frame_adjusted = sector_data_frame[(sector_data_frame.iloc[:, 0] < low_threshold) |
                                            (sector_data_frame.iloc[:, 0] > high_threshold)]
    sector = sector + ' Household with a PiN [0, ' + str(low_threshold) + '] and [' + str(high_threshold) + ', 10]'
    plotConfusionF1(data_frame_adjusted.iloc[:, 1], data_frame_adjusted.iloc[:, 2], sector, pdf)


def plotConfusionMatrix(confusion_matrix, classes, pdf, cmap=plt.cm.Blues):
    """
    Plots a confusion Matrix.

    Taken from "https://scikit-learn.org/stable/auto_examples/model_selection/
    plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py"
    """
    confusion_matrix = confusion_matrix.astype('float') / confusion_matrix.sum(axis=1)[:, np.newaxis]
    fig, ax = plt.subplots()
    im = ax.imshow(confusion_matrix, interpolation='nearest', cmap=cmap, vmin=0, vmax=1)
    ax.figure.colorbar(im, ax=ax)

    ax.set(xticks=np.arange(confusion_matrix.shape[1]),
           yticks=np.arange(confusion_matrix.shape[0]),
           xticklabels=classes, yticklabels=classes,
           ylabel='True Label',
           xlabel='Predicted Label')

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    thresh = 0.85
    for i in range(confusion_matrix.shape[0]):
        for j in range(confusion_matrix.shape[1]):
            ax.text(j, i, format(confusion_matrix[i, j], '.2f'),
                    ha="center", va="center",
                    color="white" if confusion_matrix[i, j] > thresh else "black")
    fig.tight_layout()

    pdf.savefig()
    plt.close()

    return ax