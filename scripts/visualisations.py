import geopandas as gpd
import pandas as pd
#import geoplot as gplt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from shapely.geometry import Point
from bokeh.plotting import output_file, figure, show, save
from bokeh.models import GeoJSONDataSource, LinearColorMapper, HoverTool
from bokeh.tile_providers import get_provider
from bokeh.palettes import brewer


################### IMPORT DATA #########################################

nigeria_path = '/Users/Vicky/Documents/Documents/Studium/IMPACT_project/team-2/Data/NGA_adm/NGA_adm1.shp'

nigeria = gpd.read_file(nigeria_path)

camps = pd.ExcelFile('/Users/Vicky/Documents/Documents/Studium/IMPACT_project/Data/reach_nga_msna_initial_sample.xlsx')

camp = camps.parse(0)

camp.columns = camp.iloc[0]
camp = camp.reindex(camp.index.drop(0))

camp_no_nan = camp.dropna(subset=['lon','lat'])
camp_no_nan['Coordinates'] = list(zip(camp_no_nan['lon'], camp_no_nan['lat']))
camp_no_nan['Coordinates'] = camp_no_nan['Coordinates'].apply(Point)
camp_gdf = gpd.GeoDataFrame(camp_no_nan, geometry='Coordinates')

nigeriajson = nigeria.to_json()



def plot_nigeria(output):

    output_file(output, 'Nigeria')
    # Input GeoJSON source that contains features for plotting.
    geo_json = GeoJSONDataSource(geojson=nigeriajson)
    # Define a sequential multi-hue color palette.
    palette = brewer['YlGnBu'][8]
    # Reverse color order so that dark blue is highest obesity.
    palette = palette[::-1]
    color_mapper = LinearColorMapper(palette=palette, low=0, high=40)

    # Hover tool
    hover = HoverTool(tooltips=[('Region', '@NAME_1'), ('ID', '@ID_1')])

    p = figure(title='Nigeria', toolbar_location='above', tools=[hover])
    p.patches('xs', 'ys', fill_alpha=0.7,
              fill_color={'field': 'ID_1', 'transform': color_mapper},
              line_color='black', line_width=0.5, source=geo_json)
    show(p)


def plot_nigeria_vars(output, variables=None):

    output_file(output, 'Nigeria')
    # Input GeoJSON source that contains features for plotting.
    geo_json = GeoJSONDataSource(geojson=nigeriajson)
    # Define a sequential multi-hue color palette.
    palette = brewer['YlGnBu'][8]
    # Reverse color order so that dark blue is highest obesity.
    palette = palette[::-1]
    color_mapper = LinearColorMapper(palette=palette, low=0, high=40)

    # Hover tool
    hover = HoverTool(tooltips=[('Region', '@NAME_1'), ('ID', '@ID_1')])

    p = figure(title='Nigeria', toolbar_location='above', tools=[hover])
    p.patches('xs', 'ys', fill_alpha=0.7,
              fill_color={'field': 'ID_1', 'transform': color_mapper},
              line_color='black', line_width=0.5, source=geo_json)
    p.circle(x=camp_gdf['lon'], y=camp_gdf['lat'], fill_color='red')
    show(p)




# Calculate x and y coords from polygons

def getPolyCoords(row, geom, coord_type):
    """Returns the coordinates ('x' or 'y') of edges of a Polygon exterior"""

    # Parse the exterior of the coordinate
    exterior = row[geom].exterior

    if coord_type == 'x':
        # Get the x coordinates of the exterior
        return list(exterior.coords.xy[0])
    elif coord_type == 'y':
        # Get the y coordinates of the exterior
        return list(exterior.coords.xy[1])


def getMultiPolyCoords(row, geom, coord_type):
    if row.geom_type == 'MultiPolygon':
        poly_list = list(row)

    for poly in poly_list:
        pass


# Calculate x and y coords from lines

def getLineCoords(row, geom, coord_type):
    """Returns a list of coordinates ('x' or 'y') of a LineString geometry"""
    if coord_type == 'x':
        return list(row[geom].coords.xy[0])
    elif coord_type == 'y':
        return list(row[geom].coords.xy[1])


# Calculate x and y coords from points

def getPointCoords(row, geom, coord_type):
    """Calculates coordinates ('x' or 'y') of a Point geometry"""
    if coord_type == 'x':
        return row[geom].x
    elif coord_type == 'y':
        return row[geom].y
