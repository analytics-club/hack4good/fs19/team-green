...Gif will appear shortly below this text...
![](demo/demo.gif)
To see Video demo, click [here](./demo/movie_comp.mov)
## Introduction


**Welcome to the GitLab repo of Team Green!** 

This repo was created in the context of the Hack4Good competition, which paired an NGO, IMPACT, with ETH students to gain more insights into their recently collected, cross-sectional dataset.
In 2018, IMPACT reached out to households in Nigeria to answer a questionnaire detailing their living situation and needs in different aspects of life, the Multi-Sectorial-Needs-Analysis (MSNA). 
In this work, we outline the efforts of team green to analyze the questionnaire data, which were two-fold. 
First, to help identify households in need based on (easily obtainable) demographic data rather than expensive and time-consuming questionnaires. 
Second, to provide a framework for the interactive visualization of the given data on a map.  

We hope you will be able to build on our experiences and continue with the prediction and visualisation efforts :)

All the best,

Your Team Green:

_Stephan Artmann_, 
_Viktoria de La Rochefoucauld_
_Nico Messikommer_, 
_Francesco Saltarelli_, 



## Useful Links

*  [Impact Initiatives website](http://www.impact-initiatives.org/)

  


## Terminology Clarification

In the following, we will mention Person in Need (PiN) scores versus PiN index. These two terms are not the same thing.
With the PiN score (scale of 0 - 10 per sector), we mean the absolute calculated PiN value according to the MSNA report provided by IMPACT (Annex 6).
In order to execute our predictions, however, we had to set a threshold for when a household is 'in need' versus when it is not 'in need'.
This threshold lies at a PiN score of 4, according to the MSNA report. Above, a household is considered to be 'in need'

Therefore, the PiN index will tell us solely about whether or not a household is in need or not. 

For example:
A household with a PiN score of 6 in the Wash sector would have a PiN index of 1 for this sector, i.e., it will be classified as 'in need'.

## Data Cleaning

To generate a 'clean' data table with demographic variables, PiNs and other variables used in our analysis, run the following code from the root directory:

```python
import src.cleanup.cleanup as clp
clean = clp.clean_table()   #generates a new cleaned up table class instance (WARNING: uses pre-computed data, see note below!)
clean.generate_PiN()            #calculate PiN from scratch (only necessary if PiN-data have changed)
clean.table                     #machine readable table
clean.human                     #human readable table
clean.translations              #table matching human readible strings to machine readible strings
clean.one_hot                   #one-hot encoded table
```
 
 ***Note**: This code assumes that the xls-Tables have not changed and loads pre-computed cleaned tables. If you work with new xls-Tables, or if they have changed, change the second line to*

```
clean = clp.clean_table(reload_tables=True)
```

The corresponding code files can be found in ```src/cleanup/cleanup.py```.


In order to include the additional data found on the WorldPop Project website, it is necessary to place the .tif files in ```data/external/literacy/```, ```data/external/population/``` and ```data/external/poverty/```.

To download the .tif-Files, click the following links:
*  [Literacy](https://www.worldpop.org/geodata/summary?id=1266)
*  [Population](https://www.worldpop.org/geodata/summary?id=17044)
*  [Poverty](https://www.worldpop.org/geodata/summary?id=1267)

## Predictions

The folders containing the scripts of the logistic regression and the random forest are located in ```src/```.
Each model has its own script. 
The commands required to train/predict a model can be found in the first lines of the corresponding model script.
The input features as well as the training/testing mode can be adjusted in the main function of the scripts.



## Visualisations

 If you want to run the visualisations, we recommend you to create a new environment from scratch, which is not used for the predictions,
 as the different dependencies (mainly the geographical libraries such as Geopandas, Fiona, Shapely etc.) are quite 
 delicate and on our machines only worked when all installed via conda together.
 
 So please create the environment in the following way:
 
 * navigate to the 'visualisations' folder
 
 * create a python env based on the list of packages from environment.yml within the folder

   ```conda env create -f environment.yml -n GeoEnv```

* activate the env  

  ```activate GeoEnv```
  
  
#### Now choose which version you would like to run
  
  You have the choice to run either the version that is suitable for your favourite **code editor**, or the one that you can 
  launch directly from your **jupyter notebook** workspace.
  
  Both versions require the same files:

  1. The geometry file of the wards in Nigeria (wards_geometry.pickle)
        _This is a processed version that you can generate yourself using the commented out section of code at the beginning of both files_
  2. The predictions file (PiN_test_pred.csv), which contains information about the PiN score, the PiN index and the prediction results.
        _Although we have also provided you with this file per default, you can generate it yourself using the prediction scripts._
  
  If you launch the **.py script**, then you will generate a map that is a HTML file, which you can interact with and also send to your peers.
  If you launch the **.ipynb script**, then you will generate a map inside of the jupyter notebook environment.
  
  _**Note**: We have specified a 'mode' flag, which lets you choose whether you would like to show the predictions and PiN index or the absolute PiN scores directly.
         We have taken this approach because it is not possible to create a general heatmap for two very different scales.
         Thus, in mode 'prediction' the map will show you the ground truth and predictions of the percentage of people in need in the respective sector,
          whereas mode 'pin_visualisation' will show you only the ground truth of absolute PiN values._
